CREATE TABLE IF NOT EXISTS public.notes
(
    id            BIGSERIAL PRIMARY KEY NOT NULL,
    title         VARCHAR   NOT NULL,
    description   VARCHAR   NOT NULL,
    another_note  VARCHAR,
    time_create   TIMESTAMPTZ NOT NULL DEFAULT now(),
    time_change   TIMESTAMPTZ NOT NULL DEFAULT now()
);

CREATE TABLE IF NOT EXISTS public.boards
(
    id            BIGSERIAL PRIMARY KEY NOT NULL,
    title         VARCHAR   NOT NULL,
    description   VARCHAR   NOT NULL,
    time_create   TIMESTAMPTZ NOT NULL DEFAULT now(),
    time_change   TIMESTAMPTZ NOT NULL DEFAULT now()
);

CREATE TABLE IF NOT EXISTS public.tasks
(
    id            BIGSERIAL PRIMARY KEY NOT NULL,
    board_id      BIGINT NOT NULL REFERENCES public.boards(id) ON DELETE CASCADE,
    title         VARCHAR   NOT NULL,
    description   VARCHAR   NOT NULL,
    status        VARCHAR,
    employee      VARCHAR,
    time_create   TIMESTAMPTZ NOT NULL DEFAULT now(),
    time_change   TIMESTAMPTZ NOT NULL DEFAULT now()
);

CREATE TABLE IF NOT EXISTS public.tags_on_boards
(
board_id BIGSERIAL,
tag VARCHAR NOT NULL,
FOREIGN KEY (board_id) REFERENCES public.boards (id) ON DELETE CASCADE,
PRIMARY KEY (board_id, tag)
);

CREATE TABLE IF NOT EXISTS public.tags_on_tasks
(
task_id BIGSERIAL,
tag VARCHAR NOT NULL,
FOREIGN KEY (task_id) REFERENCES public.tasks (id) ON DELETE CASCADE,
PRIMARY KEY (task_id, tag)
);

CREATE TABLE IF NOT EXISTS public.tags_on_notes
(
note_id BIGSERIAL,
tag VARCHAR NOT NULL,
FOREIGN KEY (note_id) REFERENCES public.notes (id) ON DELETE CASCADE,
PRIMARY KEY (note_id, tag)
);

CREATE TABLE IF NOT EXISTS public.status_on_boards
(
board_id BIGSERIAL,
status VARCHAR NOT NULL,
FOREIGN KEY (board_id) REFERENCES public.boards (id) ON DELETE CASCADE,
PRIMARY KEY (board_id, status)
);

CREATE TABLE IF NOT EXISTS public.schedules
(
id            BIGSERIAL PRIMARY KEY NOT NULL,
title         VARCHAR   NOT NULL,
description   VARCHAR   NOT NULL,
time_current  TIMESTAMPTZ NOT NULL DEFAULT now(),
time_create   TIMESTAMPTZ NOT NULL DEFAULT now(),
time_change   TIMESTAMPTZ NOT NULL DEFAULT now()
);

CREATE TABLE IF NOT EXISTS public.plan_on_date
(
id            BIGSERIAL PRIMARY KEY NOT NULL,
schedule_id   BIGSERIAL NOT NULL REFERENCES public.schedules(id) ON DELETE CASCADE,
date_plan     DATE NOT NULL,
plan_on_date  VARCHAR NOT NULL
);

CREATE TABLE IF NOT EXISTS public.tags_on_schedules
(
schedule_id BIGSERIAL NOT NULL REFERENCES public.schedules(id) ON DELETE CASCADE,
tag VARCHAR NOT NULL,
FOREIGN KEY (schedule_id) REFERENCES public.schedules (id) ON DELETE CASCADE,
PRIMARY KEY (schedule_id, tag)
);