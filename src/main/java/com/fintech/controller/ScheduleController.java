package com.fintech.controller;

import com.fintech.model.Schedule;
import com.fintech.service.ScheduleService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static com.fintech.exception.AppExceptions.SCHEDULE_NOT_FOUND;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@OpenAPIDefinition(
        info = @Info(
                title = "schedule-controller",
                version = "1.0",
                description = "Контроллер для взаимодействия с планировщиками"
        )
)
@RestController
@RequestMapping("/schedules")
@RequiredArgsConstructor
@Slf4j
public class ScheduleController {

    private final ScheduleService scheduleService;

    @Operation(summary = "Добавить новый планировщик",
            description = "Добавляет планировщик (Request Body) с ненулевыми значениями: названием title и описанием description; " +
                    "присваивает id, время создания timeOfCreation, время последнего изменения timeOfLastChange и " +
                    "текущее время current в БД; теги Set<String> tags, в этом методе игнорируются, и могут быть добавлены " +
                    "на планировщик другим методом")
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public void addSchedule(@RequestBody @Valid Schedule schedule) {
        log.info("ScheduleController.addSchedule.in  Schedule - {}", schedule);
        scheduleService.save(schedule);
        log.info("ScheduleController.addSchedule.out");
    }

    @Operation(summary = "Получить планировщик по id",
            description = "Возвращает планировщик с заданным id (Path Variable): с id, названием title, " +
                    "описанием description, временем последнего изменения timeOfLastChange и создания timeOfCreation, " +
                    "тегами Set<String> tags, текущее время current, предварительно обновленное. " +
                    "Если в БД нет планировщика с таким id, бросает исключение 404")
    @GetMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    public Schedule getSchedule(@PathVariable("id") long id) {
        log.info("ScheduleController.getSchedule.in  id - {}", id);
        Schedule schedule = scheduleService.findScheduleById(id);
        if (schedule == null) {
            throw SCHEDULE_NOT_FOUND.exception(String.valueOf(id));
        }
        log.info("ScheduleController.getSchedule.out  Schedule - {}", schedule);
        return schedule;
    }

    @Operation(summary = "Получить список всех планировщиков",
            description = "Возвращает список всех планировщиков: с id, названием title, описанием description, " +
                    "временем последнего изменения timeOfLastChange и создания timeOfCreation, " +
                    "текущим временем current и тегами Set<String> tags")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public List<Schedule> getAllSchedules() {
        log.info("ScheduleController.getAllSchedules.in");
        List<Schedule> schedules = scheduleService.allSchedules();
        log.info("ScheduleController.getAllSchedules.out  Schedule - {}", schedules);
        return schedules;
    }

    @Operation(summary = "Удалить планировщик по id",
            description = "Удаляет планировщик с заданным id (Path Variable), " +
                    "а так же теги Set<String> tags и планы schedulePlan, связанные с ним")
    @DeleteMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    public void deleteSchedule(@PathVariable("id") long id) {
        log.info("ScheduleController.deleteSchedule.in id - {}", id);
        scheduleService.delete(id);
        log.info("ScheduleController.deleteSchedule.out");
    }

    @Operation(summary = "Обновить планировщик по id",
            description = "Обновляет название title и описание description (not null) у планировщика (Request Body) " +
                    "с заданным id (Path Variable), обновляет время последнего изменения timeOfLastChange и " +
                    "текущее время current. Если в БД нет планировщика с таким id, бросает исключение 404")
    @PutMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    public void updateSchedule(@RequestBody @Valid Schedule schedule, @PathVariable long id) {
        log.info("ScheduleController.updateSchedule.in  Schedule - {}, id - {}.", schedule, id);
        if (scheduleService.findScheduleById(id) == null) {
            throw SCHEDULE_NOT_FOUND.exception(String.valueOf(id));
        }
        scheduleService.update(schedule, id);
        log.info("ScheduleController.updateSchedule.out");
    }

}
