package com.fintech.controller;

import com.fintech.exception.AppExceptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionsHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(AppExceptions.AppException.class)
    public ResponseEntity<AppExceptions.AppExceptionCompanion> handleAppException(AppExceptions.AppException e) {
        return ResponseEntity.status(e.appExceptionCompanion.code()).body(e.appExceptionCompanion);
    }

}