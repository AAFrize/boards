package com.fintech.controller;

import com.fintech.model.Task;
import com.fintech.service.BoardService;
import com.fintech.service.TaskService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.fintech.exception.AppExceptions.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@OpenAPIDefinition(
        info = @Info(
                title = "task-controller",
                version = "1.0",
                description = "Контроллер для взаимодействия с задачами"
        )
)
@RestController
@RequestMapping("/boards")
@RequiredArgsConstructor
@Slf4j
public class TaskController {

    private final BoardService boardService;
    private final TaskService taskService;

    @Operation(summary = "Получить задачу по id",
            description = "Возвращает задачу с заданным id (Path Variable): с id, названием title, описанием description, " +
                    "временем последнего изменения timeOfLastChange и создания timeOfCreation, id доски boardId, на которой" +
                    "она находится, статусом status и тегами Set<String> tags. Если в БД нет задачи с таким id, " +
                    "бросает исключение 404")
    @GetMapping(path = "/tasks/{id}", produces = APPLICATION_JSON_VALUE)
    public Task getTask(@PathVariable("id") long id) {
        log.info("TaskController.getTaskFromBoard.in  taskId - {}", id);
        Task task = taskService.findTaskById(id);
        if (task == null) {
            throw TASK_NOT_FOUND.exception(String.valueOf(id));
        }
        log.info("TaskController.getTaskFromBoard.out  Task - {}", task);
        return task;
    }

    @Operation(summary = "Удалить задачу по id",
            description = "Удаляет задачу с заданным id (Path Variable), а также связанные с ней теги")
    @DeleteMapping(path = "/tasks/{id}", produces = APPLICATION_JSON_VALUE)
    public void deleteTask(@PathVariable("id") long id) {
        log.info("TaskController.deleteTaskFromBoard.in  taskId - {}", id);
        taskService.deleteTask(id);
        log.info("TaskController.deleteTaskFromBoard.out");
    }


    @Operation(summary = "Обновить задачу по id",
            description = "Обновляет название title и описание description (not null), имя сотрудника nameOfEmployee, " +
                    "статус status (если у доски нет заданного статуса, ему будет присвоено значение null) " +
                    "у задачи (Request Body) на доске с id boardId (Path Variable) с заданным id (Path Variable)," +
                    " обновляет время последнего изменения timeOfLastChange. Если в БД нет задачи с таким id, " +
                    "бросает исключение 404")
    @PutMapping(path = "/{boardId}/tasks/{id}", produces = APPLICATION_JSON_VALUE)
    public void updateTaskOnBoard(@PathVariable long boardId, @RequestBody @Valid Task task, @PathVariable long id) {
        log.info("TaskController.updateTaskOnBoard.in  Board id - {}; Task - {}; Task id - {}.", boardId, task, id);
        if (taskService.findTaskById(id) == null) {
            throw TASK_NOT_FOUND.exception(String.valueOf(id));
        }
        taskService.setStatus(boardId, id, task.getStatus());
        taskService.updateTask(task, id);
        log.info("TaskController.updateTaskOnBoard.out");
    }

    @Operation(summary = "Переместить задачу на другую доску по id",
            description = "Обновляет id доски boardId на newBoardId (RequestParam) у задачи с id (Path Variable), " +
                    "если у новой доски нет статуса, указанного на задаче, ему будет присвоено значение null; " +
                    "обновляет время последнего изменения timeOfLastChange. Если в БД нет задачи с таким id или нет " +
                    "доски с id newBoardId, бросает исключение 404")
    @PutMapping(path = "/tasks/{id}/replace", produces = APPLICATION_JSON_VALUE)
    public void replaceTask(@PathVariable long id, @RequestParam long newBoardId) {
        log.info("TaskController.replaceTask.in  to Board id -{}, Task id - {}.", newBoardId, id);
        Task task = taskService.findTaskById(id);
        if (task == null) {
            throw TASK_NOT_FOUND.exception(String.valueOf(id));
        }
        if (boardService.findBoard(newBoardId) == null) {
            throw BOARD_NOT_FOUND.exception(String.valueOf(newBoardId));
        }
        taskService.replaceTaskToAnotherBoard(newBoardId, task, id);
        log.info("TaskController.replaceTask.out");
    }

}
