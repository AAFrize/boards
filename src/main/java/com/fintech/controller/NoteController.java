package com.fintech.controller;

import com.fintech.model.Note;
import com.fintech.service.NoteService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static com.fintech.exception.AppExceptions.NOTE_NOT_FOUND;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@OpenAPIDefinition(
        info = @Info(
                title = "note-controller",
                version = "1.0",
                description = "Контроллер для взаимодействия с заметками"
        )
)
@RestController
@RequestMapping("/notes")
@RequiredArgsConstructor
@Slf4j
public class NoteController {

    private final NoteService noteService;

    @Operation(summary = "Добавить новую заметку",
            description = "Добавляет заметку (Request Body) с ненулевыми значениями: названием title и описанием description; " +
                    "может сохранить ссылку на другую заметку (на её id) anotherNote, если этой заметки не существует в БД, " +
                    "будет выброшено исключение 400; присваивает id, время создания timeOfCreation и время последнего изменения timeOfLastChange " +
                    "в БД; теги Set<String> tags, в этом методе игнорируются, и могут быть добавлены на заметку другим методом")
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public void addNote(@RequestBody @Valid Note note) {
        log.info("NoteController.addNote.in Note - {}.", note);
        noteService.save(note);
        log.info("NoteController.addNote.out");
    }

    @Operation(summary = "Получить заметку по id",
            description = "Возвращает заметку с заданным id (Path Variable): с id, названием title, описанием description, " +
                    "временем последнего изменения timeOfLastChange и создания timeOfCreation, " +
                    "ссылкой на другую заметку anotherNote и тегами Set<String> tags. " +
                    "Если в БД нет заметки с таким id, бросает исключение 404")
    @GetMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    public Note getNote(@PathVariable long id) {
        log.info("NoteController.getNote.in  id - {}.", id);
        Note note = noteService.findNote(id);
        if (note == null) {
            throw NOTE_NOT_FOUND.exception(String.valueOf(id));
        }
        log.info("NoteController.getNote.out  Note - {}.", note);
        return note;
    }

    @Operation(summary = "Получить список всех заметок",
            description = "Возвращает список всех заметок: с id, названием title, описанием description, " +
                    "временем последнего изменения timeOfLastChange и создания timeOfCreation, " +
                    "ссылкой на другую заметку anotherNote и тегами Set<String> tags")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public List<Note> getAllNotes() {
        log.info("NoteController.getNotes.in");
        List<Note> notes = noteService.allNotes();
        log.info("NoteController.getNote.out  Notes - {}.", notes);
        return notes;
    }

    @Operation(summary = "Удалить заметку по id",
            description = "Удаляет заметку с заданным id (Path Variable), " +
                    "а так же теги Set<String> tags, связанные с ней")
    @DeleteMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    public void deleteNote(@PathVariable long id) {
        log.info("NoteController.deleteNote.in  id - {}", id);
        noteService.delete(id);
        log.info("NoteController.deleteNote.out");
    }


    @Operation(summary = "Обновить заметку по id",
            description = "Обновляет название title и описание description (not null), ссылку на другую заметку anotherNote" +
                    "(если этой заметки не существует в БД, будет выброшено исключение 400) у заметки (Request Body) " +
                    "с заданным id (Path Variable), обновляет время последнего изменения timeOfLastChange. " +
                    "Если в БД нет заметки с таким id, бросает исключение 404")
    @PutMapping(path = "/{noteId}", produces = APPLICATION_JSON_VALUE)
    public void updateNote(@RequestBody @Valid Note note, @PathVariable long noteId) {
        log.info("NoteController.updateNote.in Note - {}, id - {}", note, noteId);
        Note oldNote = noteService.findNote(noteId);
        if (oldNote == null) {
            throw NOTE_NOT_FOUND.exception(String.valueOf(noteId));
        }
        noteService.update(note, noteId);
        log.info("NoteController.updateNote.out");
    }

}
