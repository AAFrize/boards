package com.fintech.controller;

import com.fintech.model.SchedulePlan;
import com.fintech.service.SchedulePlanService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Set;

import static com.fintech.exception.AppExceptions.FIRST_DATE_MUST_BE_EARLIER;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@OpenAPIDefinition(
        info = @Info(
                title = "schedule-plan-controller",
                version = "1.0",
                description = "Контроллер для взаимодействия с планами"
        )
)
@RestController
@RequestMapping("/schedules")
@RequiredArgsConstructor
@Slf4j
public class SchedulePlanController {

    private final SchedulePlanService schedulePlanService;

    @Operation(summary = "Добавить план в планировщик с id",
            description = "Добавляет план (Request Body) schedulePlan на планировщие с заданным (Path Variable) id с " +
                    "ненулевыми значениями: планируемой датой date и мероприятием plan; присваивает id и " +
                    "id планировщика scheduledId, на которую план добавляется.")
    @PostMapping(path = "/{id}/plans", consumes = APPLICATION_JSON_VALUE)
    public void addPlanToDate(@PathVariable long id, @RequestBody @Valid SchedulePlan schedulePlan) {
        log.info("SchedulePlanController.addPlanToDate.in  scheduleId - {}, date - {}, plans - {}",
                id, schedulePlan.getDate(), schedulePlan.getPlan());
        schedulePlanService.addPlanToSchedule(id, schedulePlan.getDate(), schedulePlan.getPlan());
        log.info("SchedulePlanController.addPlanToDate.out");
    }

    @Operation(summary = "Получить список планов по id планировщика",
            description = "Возвращает список всех планов планировщика с заданным id (Path Variable): с id, " +
                    "датами date и мероприятиями plan.")
    @GetMapping(path = "/{id}/plans", produces = APPLICATION_JSON_VALUE)
    public Set<SchedulePlan> getAllPlansInSchedule(@PathVariable long id) {
        log.info("SchedulePlanController.getAllPlansInSchedule.in  id - {}", id);
        Set<SchedulePlan> result = schedulePlanService.allPlansOnSchedule(id);
        log.info("SchedulePlanController.getAllPlansInSchedule.out  result - {}", result);
        return result;
    }

    @Operation(summary = "Получить список планов по id планировщика на заданный интервал времени",
            description = "Возвращает список планов планировщика с заданным id (Path Variable) во временном" +
                    "промежутке с даты fromDate (Request Param) по дату toDate(RequestParam) включительно: с id, " +
                    "датами date и мероприятиями plan. Даты нобходимо вводить в формате yyyy-MM-dd. " +
                    "Для получения мероприятий на конкретную дату, необходимо указать одинаковые fromDate и toDate. " +
                    "Если указанная дата fromDate позже, чем toDate, будет выброшено исключение 400")
    @GetMapping(path = "/{id}/plans/dates", produces = APPLICATION_JSON_VALUE)
    public Set<SchedulePlan> getPlansBetweenTwoDate(@PathVariable("id") long id,
                                                    @RequestParam("fromDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fromDate,
                                                    @RequestParam("toDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate toDate) {
        log.info("SchedulePlanController.getPlansBetweenTwoDate.in  id - {}, fromDate - {}, toDate - {}", id, fromDate, toDate);
        if (fromDate.isAfter(toDate)) {
            throw FIRST_DATE_MUST_BE_EARLIER.exception("first date - " + fromDate + "; second date - " + toDate);
        }
        Set<SchedulePlan> result = schedulePlanService.planBetweenDates(id, fromDate, toDate);
        log.info("SchedulePlanController.getPlansBetweenTwoDate.out  result- {}", result);
        return result;
    }

    @Operation(summary = "Удалить план по id",
            description = "Удаляет план с планировщика с id (Path Variable) по planId (Path Variable).")
    @DeleteMapping(path = "/plans/{planId}", produces = APPLICATION_JSON_VALUE)
    public void deletePlanById(@PathVariable("planId") long planId) {
        log.info("SchedulePlanController.deletePlanById.in  Plan id - {}", planId);
        schedulePlanService.deletePlanFromId(planId);
        log.info("SchedulePlanController.deletePlanById.out");
    }

}
