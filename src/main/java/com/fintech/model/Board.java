package com.fintech.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@RequiredArgsConstructor
public class Board {

    private long id;
    @NotNull
    private String title;
    @NotNull
    private String description;
    private LocalDateTime timeOfLastChange;
    private LocalDateTime timeOfCreation;
    private Set<String> status;
    private Set<String> tags;
    private Set<Task> tasks;

    public Board(String title, String description) {
        this.title = title;
        this.description = description;
    }

}
