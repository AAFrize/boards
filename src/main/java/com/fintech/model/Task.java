package com.fintech.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@RequiredArgsConstructor
public class Task {

    private long id;
    private long boardId;
    @NotNull
    private String title;
    @NotNull
    private String description;
    private Set<String> tags;
    private String status;
    private String nameOfEmployee;
    private LocalDateTime timeOfLastChange;
    private LocalDateTime timeOfCreation;

    public Task(String title, String description, Set<String> tags, String status, String nameOfEmployee) {
        this.title = title;
        this.description = description;
        this.tags = tags;
        this.status = status;
        this.nameOfEmployee = nameOfEmployee;
    }

    public Task(long id, String title, String description, String nameOfEmployee) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.nameOfEmployee = nameOfEmployee;
    }

    public Task(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public Task(String title, String description, String status) {
        this.title = title;
        this.description = description;
        this.status = status;
    }

    public Task(String title, String description, long boardId) {
        this.title = title;
        this.description = description;
        this.boardId = boardId;
    }

    public Task(long boardId, String title, String description, String status, String nameOfEmployee) {
        this.boardId = boardId;
        this.title = title;
        this.description = description;
        this.status = status;
        this.nameOfEmployee = nameOfEmployee;
    }
}
