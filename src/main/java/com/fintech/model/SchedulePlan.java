package com.fintech.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@RequiredArgsConstructor
public class SchedulePlan {

    private long id;
    @NotNull
    private LocalDate date;
    @NotNull
    private String plan;

    public SchedulePlan(LocalDate date, String plan) {
        this.date = date;
        this.plan = plan;
    }
}
