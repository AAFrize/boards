package com.fintech.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@RequiredArgsConstructor
public class Note {

    private long id;
    @NotNull
    private String title;
    @NotNull
    private String description;
    private Set<String> tags;
    private long anotherNote;
    private LocalDateTime timeOfLastChange;
    private LocalDateTime timeOfCreation;

    public Note(long id, String title, String description, Set<String> tags, long anotherNote) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.tags = tags;
        this.anotherNote = anotherNote;
    }

    public Note(String title, String description, Set<String> tags, long anotherNote) {
        this.title = title;
        this.description = description;
        this.tags = tags;
        this.anotherNote = anotherNote;
    }

    public Note(long id, String title, String description, Set<String> tags) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.tags = tags;
    }

    public Note(String title, String description, Set<String> tags) {
        this.title = title;
        this.description = description;
        this.tags = tags;
    }

    public Note(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public Note(String title, String description, long anotherNote) {
        this.title = title;
        this.description = description;
        this.anotherNote = anotherNote;
    }
}
