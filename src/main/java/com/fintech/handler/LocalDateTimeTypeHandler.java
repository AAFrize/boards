package com.fintech.handler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class LocalDateTimeTypeHandler extends BaseTypeHandler<LocalDateTime> {
    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, LocalDateTime localDateTime, JdbcType jdbcType) throws SQLException {
        Instant instant = localDateTime.toInstant(ZoneOffset.UTC);
        Timestamp ts = Timestamp.from(instant);
        ps.setTimestamp(i, ts);
    }

    @Override
    public LocalDateTime getNullableResult(ResultSet rs, String s) throws SQLException {
        return rs.getTimestamp(s).toLocalDateTime();
    }

    @Override
    public LocalDateTime getNullableResult(ResultSet rs, int i) throws SQLException {
        return rs.getTimestamp(i).toLocalDateTime();
    }

    @Override
    public LocalDateTime getNullableResult(CallableStatement cs, int i) throws SQLException {
        return cs.getTimestamp(i).toLocalDateTime();
    }
}
