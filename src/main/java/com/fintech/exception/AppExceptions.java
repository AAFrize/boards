package com.fintech.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;

public enum AppExceptions {

    NOTE_NOT_FOUND("Note not found", 404),
    ANOTHER_NOTE_NOT_FOUND("Note refers to a non-existent note", 400),
    BOARD_NOT_FOUND("Board not found", 404),
    TASK_NOT_FOUND("Task not found", 404),
    FIRST_DATE_MUST_BE_EARLIER("First date must be earlier than second date", 400),
    TAG_NOT_FOUND("Tag not found", 404),
    TAG_ON_NOTE_NOT_FOUND("There are no notes with this tag", 404),
    TAG_ON_BOARD_NOT_FOUND("There are no boards with this tag", 404),
    TAG_ON_TASK_NOT_FOUND("There are no tasks with this tag", 404),
    TAG_ON_SCHEDULE_NOT_FOUND("There are no schedules with this tag", 404),
    SCHEDULE_NOT_FOUND("Schedule not found", 404);

    private final String message;
    private final int code;

    AppExceptions(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public AppException exception(String args) {
        return new AppException(this, args);
    }

    public static class AppException extends RuntimeException {
        public final AppExceptionCompanion appExceptionCompanion;

        AppException(AppExceptions error, String message) {
            super(error.message + " : " + message);
            this.appExceptionCompanion = new AppExceptionCompanion(error.code, error.message + " : " + message);
        }
    }

    public static record AppExceptionCompanion(@JsonIgnore int code, String message) {
    }
}
