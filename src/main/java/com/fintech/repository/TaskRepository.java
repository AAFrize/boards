package com.fintech.repository;

import com.fintech.model.Task;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TaskRepository {

    void save(Task task);

    void update(Task task);

    void changeStatus(long id, String status);

    void delete(long id);

    List<Task> findAll();

    Task findByTaskId(long id);

    List<Task> findByBoardId(long boardId);

    void replace(long id, long newBoardId, String status);

    List<Task> findTasksById(List<Long> id);

}
