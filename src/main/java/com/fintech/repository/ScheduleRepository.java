package com.fintech.repository;

import com.fintech.model.Schedule;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ScheduleRepository {

    void save(Schedule schedule);

    void update(Schedule schedule);

    void delete(long id);

    List<Schedule> findAll();

    Schedule findById(long id);

    List<Schedule> findSchedulesById(List<Long> id);
}
