package com.fintech.repository;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TagRepository {

    void saveToBoard(long id, String tag);

    void saveToTask(long id, String tag);

    void saveToNote(long id, String tag);

    void saveToSchedule(long id, String tag);

    void deleteFromBoard(long id, String tag);

    void deleteFromTask(long id, String tag);

    void deleteFromNote(long id, String tag);

    void deleteFromSchedule(long id, String tag);

    List<String> findAll();

    List<Long> findBoardsByTag(String tag);

    List<Long> findNotesByTag(String tag);

    List<Long> findTasksByTag(String tag);

    List<Long> findSchedulesByTag(String tag);

}
