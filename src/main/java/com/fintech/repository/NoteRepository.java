package com.fintech.repository;

import com.fintech.model.Note;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface NoteRepository {

    void save(Note note);

    void update(Note note);

    void delete(long id);

    List<Note> findAll();

    Note findById(long id);

    List<Note> findNotesById(List<Long> id);

}
