package com.fintech.service;

import com.fintech.model.SchedulePlan;
import com.fintech.repository.SchedulePlanRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class SchedulePlanService {

    private final SchedulePlanRepository planRepository;

    public void addPlanToSchedule(long scheduleId, LocalDate date, String plan) {
        planRepository.save(scheduleId, date, plan);
    }

    public void deletePlanFromId(long planId) {
        planRepository.delete(planId);
    }

    public Set<SchedulePlan> allPlansOnSchedule(long scheduleId) {
        return Set.copyOf(planRepository.findByScheduleId(scheduleId));
    }

    public Set<SchedulePlan> planBetweenDates(long scheduleId, LocalDate fromDate, LocalDate toDate) {
        return Set.copyOf(planRepository.findByScheduleIdAndTwoDate(scheduleId, fromDate, toDate));
    }

    public List<SchedulePlan> allPlans() {
        return planRepository.findAll();
    }
}
