package com.fintech.service;

import com.fintech.model.Board;
import com.fintech.model.Note;
import com.fintech.model.Schedule;
import com.fintech.model.Task;
import com.fintech.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class TagService {

    private final NoteRepository noteRepository;
    private final TagRepository tagRepository;
    private final BoardRepository boardRepository;
    private final TaskRepository taskRepository;
    private final ScheduleRepository scheduleRepository;

    @CacheEvict(value = "boards", key = "#boardId")
    public void addTagToBoard(long boardId, String tag) {
        tagRepository.saveToBoard(boardId, tag.toLowerCase());
    }

    @CacheEvict(value = "tasks", key = "#taskId")
    public void addTagToTask(long taskId, String tag) {
        tagRepository.saveToTask(taskId, tag.toLowerCase());
    }

    @CacheEvict(value = "notes", key = "#noteId")
    public void addTagToNote(long noteId, String tag) {
        tagRepository.saveToNote(noteId, tag.toLowerCase());
    }

    @CacheEvict(value = "schedules", key = "#scheduleId")
    public void addTagToSchedule(long scheduleId, String tag) {
        tagRepository.saveToSchedule(scheduleId, tag.toLowerCase());
    }

    public List<Board> findBoardsByTag(String tag) {
        List<Long> boardsId = tagRepository.findBoardsByTag(tag.toLowerCase());
        return boardRepository.findBoardsById(boardsId);
    }

    public List<Task> findTasksByTag(String tag) {
        List<Long> tasksId = tagRepository.findTasksByTag(tag.toLowerCase());
        return taskRepository.findTasksById(tasksId);
    }

    public List<Note> findNotesByTag(String tag) {
        List<Long> notesId = tagRepository.findNotesByTag(tag.toLowerCase());
        return noteRepository.findNotesById(notesId);
    }

    public List<Schedule> findSchedulesByTag(String tag) {
        List<Long> schedulesId = tagRepository.findSchedulesByTag(tag.toLowerCase());
        return scheduleRepository.findSchedulesById(schedulesId);
    }

    public List<Object> findSomethingByTag(String tag) {
        List<Object> list = new ArrayList<>(findBoardsByTag(tag));
        list.addAll(findTasksByTag(tag));
        list.addAll(findNotesByTag(tag));
        list.addAll(findSchedulesByTag(tag));
        return list;
    }

    public Set<String> allTags() {
        return Set.copyOf(tagRepository.findAll());
    }

    @CacheEvict(value = "notes", key = "#noteId")
    public void deleteTagFromNote(long noteId, String tag) {
        tagRepository.deleteFromNote(noteId, tag.toLowerCase());
    }

    @CacheEvict(value = "boards", key = "#boardId")
    public void deleteTagFromBoard(long boardId, String tag) {
        tagRepository.deleteFromBoard(boardId, tag.toLowerCase());
    }

    @CacheEvict(value = "tasks", key = "#taskId")
    public void deleteTagFromTask(long taskId, String tag) {
        tagRepository.deleteFromTask(taskId, tag.toLowerCase());
    }

    @CacheEvict(value = "schedules", key = "#scheduleId")
    public void deleteTagFromSchedule(long scheduleId, String tag) {
        tagRepository.deleteFromSchedule(scheduleId, tag.toLowerCase());
    }
}
