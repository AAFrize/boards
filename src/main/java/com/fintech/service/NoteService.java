package com.fintech.service;

import com.fintech.model.Note;
import com.fintech.repository.NoteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.fintech.exception.AppExceptions.ANOTHER_NOTE_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class NoteService {

    private final NoteRepository repository;

    public void save(Note note) {
        if (note.getAnotherNote() == 0) {
            repository.save(note);
        } else if (repository.findAll().stream()
                .noneMatch(n -> note.getAnotherNote() == n.getId())) {
            throw ANOTHER_NOTE_NOT_FOUND.exception(String.valueOf(note.getAnotherNote()));
        } else {
            repository.save(note);
        }
    }

    @CacheEvict(value = "notes", key = "#noteId")
    public void update(Note note, long noteId) {
        note.setId(noteId);
        if (note.getAnotherNote() == 0) {
            repository.update(note);
        } else if (repository.findAll().stream()
                .noneMatch(n -> note.getAnotherNote() == n.getId())) {
            throw ANOTHER_NOTE_NOT_FOUND.exception(String.valueOf(note.getAnotherNote()));
        } else {
            repository.update(note);
        }
    }

    @CacheEvict(value = "notes", key = "#noteId")
    public void delete(long noteId) {
        repository.delete(noteId);
    }

    public List<Note> allNotes() {
        return repository.findAll();
    }

    @Cacheable(value = "notes", key = "#id", unless = "#result == null")
    public Note findNote(long id) {
        return repository.findById(id);
    }

}
