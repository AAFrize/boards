package com.fintech.service;

import com.fintech.model.Schedule;
import com.fintech.repository.ScheduleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ScheduleService {

    private final ScheduleRepository repository;

    public void save(Schedule schedule) {
        repository.save(schedule);
    }

    @CacheEvict(value = "schedules", key = "#scheduleId")
    public void update(Schedule schedule, long scheduleId) {
        schedule.setId(scheduleId);
        repository.update(schedule);
    }

    @CacheEvict(value = "schedules", key = "#id")
    public void delete(long id) {
        repository.delete(id);
    }

    @Cacheable(value = "schedules", key = "#id", unless = "#result == null")
    public Schedule findScheduleById(long id) {
        return repository.findById(id);
    }

    public List<Schedule> allSchedules() {
        return repository.findAll();
    }
}
