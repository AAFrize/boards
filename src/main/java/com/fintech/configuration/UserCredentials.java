package com.fintech.configuration;

import lombok.Data;

@Data
public class UserCredentials {

    private String username;
    private String password;
    private Roles role;

}

