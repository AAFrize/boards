package com.fintech.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fintech.model.Schedule;
import com.fintech.model.SchedulePlan;
import com.fintech.service.SchedulePlanService;
import com.fintech.service.ScheduleService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class SchedulePlanControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private SchedulePlanController schedulePlanController;
    @Autowired
    private SchedulePlanService schedulePlanService;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private ScheduleService scheduleService;

    private static final ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    SchedulePlan normPlan = prepareSchedulePlan(LocalDate.now(), "Pass all tests");
    SchedulePlan normPlan2 = prepareSchedulePlan(LocalDate.now().plusDays(1), "Pass the tests on getAll and getBetweenDates");
    Schedule normSchedule = prepareSchedule("Schedule", "Some schedule");

    @AfterEach
    void resetDb() {
        scheduleService.allSchedules().forEach(schedule -> scheduleService.delete(schedule.getId()));
        schedulePlanService.allPlans().forEach(plan -> schedulePlanService.deletePlanFromId(plan.getId()));
    }

    @Test
    void validAddPlanOnSchedule() throws Exception {
        scheduleService.save(normSchedule);
        String sql = "SELECT * FROM public.schedules WHERE title = '" + normSchedule.getTitle()
                + "' AND description = '" + normSchedule.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String json = mapper.writeValueAsString(normPlan);
        mockMvc.perform(post("/schedules/{id}/plans", id).content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
        String sql2 = "SELECT * FROM public.plan_on_date WHERE date_plan = '" + normPlan.getDate()
                + "' AND plan_on_date = '" + normPlan.getPlan() + "'";
        SchedulePlan entry = jdbcTemplate.queryForObject(sql2, (rs, rowNum) ->
                new SchedulePlan(rs.getDate("date_plan").toLocalDate(),
                        rs.getString("plan_on_date")));
        assertEquals(normPlan, entry);
    }

    @Test
    void invalidAddPlanOnScheduleBadRequest() throws Exception {
        scheduleService.save(normSchedule);
        String sql = "SELECT * FROM public.schedules WHERE title = '" + normSchedule.getTitle()
                + "' AND description = '" + normSchedule.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        normPlan.setDate(null);
        String json = mapper.writeValueAsString(normPlan);
        mockMvc.perform(post("/schedules/{id}/plans", id).content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    void validGetAllPlansInSchedule() throws Exception {
        scheduleService.save(normSchedule);
        String sql = "SELECT * FROM public.schedules WHERE title = '" + normSchedule.getTitle()
                + "' AND description = '" + normSchedule.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        schedulePlanService.addPlanToSchedule(id, normPlan.getDate(), normPlan.getPlan());
        schedulePlanService.addPlanToSchedule(id, normPlan2.getDate(), normPlan2.getPlan());
        mockMvc.perform(get("/schedules/{id}/plans", id).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andExpect(jsonPath("$..date",
                        hasItems(normPlan.getDate().toString(), normPlan2.getDate().toString())))
                .andExpect(jsonPath("$..plan", hasItems(normPlan.getPlan(), normPlan2.getPlan())));
    }

    @Test
    void validGetPlansBetweenDays() throws Exception {
        scheduleService.save(normSchedule);
        String sql = "SELECT * FROM public.schedules WHERE title = '" + normSchedule.getTitle()
                + "' AND description = '" + normSchedule.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        schedulePlanService.addPlanToSchedule(id, normPlan.getDate(), normPlan.getPlan());
        schedulePlanService.addPlanToSchedule(id, normPlan2.getDate(), normPlan2.getPlan());
        LocalDate fromDate = LocalDate.now().minus(1, ChronoUnit.DAYS);
        LocalDate toDate = LocalDate.now().plus(2, ChronoUnit.DAYS);
        mockMvc.perform(get("/schedules/{scheduleId}/plans/dates?fromDate={fromDate}&toDate={toDate}",
                        id, fromDate, toDate).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andExpect(jsonPath("$..date",
                        hasItems(normPlan.getDate().toString(), normPlan2.getDate().toString())))
                .andExpect(jsonPath("$..plan", hasItems(normPlan.getPlan(), normPlan2.getPlan())));
    }

    @Test
    void validGetPlansOnOneDay() throws Exception {
        scheduleService.save(normSchedule);
        String sql = "SELECT * FROM public.schedules WHERE title = '" + normSchedule.getTitle()
                + "' AND description = '" + normSchedule.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        schedulePlanService.addPlanToSchedule(id, normPlan.getDate(), normPlan.getPlan());
        schedulePlanService.addPlanToSchedule(id, normPlan2.getDate(), normPlan2.getPlan());
        LocalDate fromDate = LocalDate.now();
        mockMvc.perform(get("/schedules/{scheduleId}/plans/dates?fromDate={fromDate}&toDate={toDate}",
                        id, fromDate, fromDate).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andExpect(jsonPath("$..date",
                        allOf(hasItem(normPlan.getDate().toString()), not(hasItem(normPlan2.getDate().toString())))))
                .andExpect(jsonPath("$..plan", allOf(hasItem(normPlan.getPlan()), not(hasItem(normPlan2.getPlan())))));
    }

    @Test
    void invalidGetPlansBetweenDaysIncorrectInterval() throws Exception {
        scheduleService.save(normSchedule);
        String sql = "SELECT * FROM public.schedules WHERE title = '" + normSchedule.getTitle()
                + "' AND description = '" + normSchedule.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        schedulePlanService.addPlanToSchedule(id, normPlan.getDate(), normPlan.getPlan());
        schedulePlanService.addPlanToSchedule(id, normPlan2.getDate(), normPlan2.getPlan());
        LocalDate fromDate = LocalDate.now();
        LocalDate toDate = LocalDate.now().minus(1, ChronoUnit.DAYS);
        mockMvc.perform(get("/schedules/{scheduleId}/plans/dates?fromDate={fromDate}&toDate={toDate}",
                        id, fromDate, toDate).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest()).andExpect(jsonPath("$.message")
                        .value("First date must be earlier than second date : "
                                + "first date - " + fromDate + "; second date - " + toDate));
    }

    @Test
    void invalidGetPlansBetweenDaysBadRequest() throws Exception {
        scheduleService.save(normSchedule);
        String sql = "SELECT * FROM public.schedules WHERE title = '" + normSchedule.getTitle()
                + "' AND description = '" + normSchedule.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        schedulePlanService.addPlanToSchedule(id, normPlan.getDate(), normPlan.getPlan());
        LocalDate toDate = LocalDate.now().minus(1, ChronoUnit.DAYS);
        mockMvc.perform(get("/schedules/{scheduleId}/plans/dates?fromDate={fromDate}&toDate={toDate}",
                        id, null, toDate).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    void validDeletePlan() throws Exception {
        scheduleService.save(normSchedule);
        String sql = "SELECT * FROM public.schedules WHERE title = '" + normSchedule.getTitle()
                + "' AND description = '" + normSchedule.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        schedulePlanService.addPlanToSchedule(id, normPlan.getDate(), normPlan.getPlan());
        String sql2 = "SELECT * FROM public.plan_on_date WHERE date_plan = '" + normPlan.getDate()
                + "' AND plan_on_date = '" + normPlan.getPlan() + "'";
        long planId = (Long) jdbcTemplate.query(sql2, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        mockMvc.perform(delete("/schedules/plans/{planId}", planId)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
        var newId = jdbcTemplate.query(sql2, (rs, rowNum) -> rs.getLong("id")).toArray();
        assertTrue(List.of(newId).isEmpty());
    }

    private static SchedulePlan prepareSchedulePlan(LocalDate date, String plan) {
        return new SchedulePlan(date, plan);
    }

    private static Schedule prepareSchedule(String title, String description) {
        return new Schedule(title, description);
    }

}