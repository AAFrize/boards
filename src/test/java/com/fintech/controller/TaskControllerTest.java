package com.fintech.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fintech.model.Board;
import com.fintech.model.Task;
import com.fintech.service.BoardService;
import com.fintech.service.TaskService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class TaskControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private BoardController boardController;
    @Autowired
    private BoardService boardService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final ObjectMapper mapper = new ObjectMapper();

    private Board normBoard = prepareBoard("Board", "Some board");
    private Board normBoard2 = prepareBoard("Board 2", "Some board 2");
    private Task normTask = prepareTask("Task", "Some task");

    @AfterEach
    void resetDb() {
        boardService.allBoards().forEach(board -> boardService.delete(board.getId()));
    }

    @Test
    void validGetTask() throws Exception {
        boardService.save(normBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + normBoard.getTitle()
                + "' AND description = '" + normBoard.getDescription() + "'";
        long boardId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        taskService.addTaskToBoard(boardId, normTask);
        String sql2 = "SELECT * FROM public.tasks WHERE title = '" + normTask.getTitle()
                + "' AND description = '" + normTask.getDescription() + "'";
        long taskId = (Long) jdbcTemplate.query(sql2, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        mockMvc.perform(get("/boards/tasks/{taskId}", taskId).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andExpect(jsonPath("$.id").value(taskId));
    }

    @Test
    void invalidGetTaskNotFound() throws Exception {
        long taskId = 0;
        mockMvc.perform(get("/boards/tasks/{taskId}", taskId).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound()).andExpect(jsonPath("$.message")
                        .value("Task not found : " + taskId));
    }

    @Test
    void validDeleteTask() throws Exception {
        boardService.save(normBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + normBoard.getTitle()
                + "' AND description = '" + normBoard.getDescription() + "'";
        long boardId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        taskService.addTaskToBoard(boardId, normTask);
        String sql2 = "SELECT * FROM public.tasks WHERE title = '" + normTask.getTitle()
                + "' AND description = '" + normTask.getDescription() + "'";
        long taskId = (Long) jdbcTemplate.query(sql2, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        mockMvc.perform(delete("/boards/tasks/{taskId}", taskId).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
        var newId = jdbcTemplate.query(sql2, (rs, rowNum) -> rs.getLong("id")).toArray();
        assertTrue(List.of(newId).isEmpty());
    }

    @Test
    void validUpdateTaskSameStatus() throws Exception {
        boardService.save(normBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + normBoard.getTitle()
                + "' AND description = '" + normBoard.getDescription() + "'";
        long boardId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String status = "IN_PROGRESS";
        boardService.addStatus(boardId, status);
        taskService.addTaskToBoard(boardId, normTask);
        String sql2 = "SELECT * FROM public.tasks WHERE title = '" + normTask.getTitle()
                + "' AND description = '" + normTask.getDescription() + "'";
        long taskId = (Long) jdbcTemplate.query(sql2, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String employee = "Anny";
        normTask.setNameOfEmployee(employee);
        normTask.setStatus(status);
        String json = mapper.writeValueAsString(normTask);
        mockMvc.perform(put("/boards/{boardId}/tasks/{taskId}", boardId, taskId).content(json)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
        Task entry = (Task) jdbcTemplate.query(sql2, (rs, rowNum) -> new Task(rs.getLong("board_id"),
                rs.getString("title"), rs.getString("description"),
                rs.getString("status"), rs.getString("employee"))).toArray()[0];
        assertEquals(normTask, entry);
    }

    @Test
    void validUpdateTaskDifferentStatus() throws Exception {
        boardService.save(normBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + normBoard.getTitle()
                + "' AND description = '" + normBoard.getDescription() + "'";
        long boardId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String statusBoard = "IN_PROGRESS";
        String statusTask = "Another status";
        boardService.addStatus(boardId, statusBoard);
        taskService.addTaskToBoard(boardId, normTask);
        String sql2 = "SELECT * FROM public.tasks WHERE title = '" + normTask.getTitle()
                + "' AND description = '" + normTask.getDescription() + "'";
        long taskId = (Long) jdbcTemplate.query(sql2, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String employee = "Anny";
        normTask.setNameOfEmployee(employee);
        normTask.setStatus(statusTask);
        String json = mapper.writeValueAsString(normTask);
        mockMvc.perform(put("/boards/{boardId}/tasks/{taskId}", boardId, taskId).content(json)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
        Task entry = (Task) jdbcTemplate.query(sql2, (rs, rowNum) -> new Task(rs.getLong("board_id"),
                rs.getString("title"), rs.getString("description"),
                rs.getString("status"), rs.getString("employee"))).toArray()[0];
        normTask.setStatus(null);
        assertEquals(normTask, entry);
    }

    @Test
    void invalidUpdateTaskNotFound() throws Exception {
        boardService.save(normBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + normBoard.getTitle()
                + "' AND description = '" + normBoard.getDescription() + "'";
        long boardId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String json = mapper.writeValueAsString(normTask);
        long taskId = 0;
        mockMvc.perform(put("/boards/{boardId}/tasks/{taskId}", boardId, taskId).content(json)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Task not found : " + taskId));
    }

    @Test
    void invalidUpdateTaskBadRequest() throws Exception {
        boardService.save(normBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + normBoard.getTitle()
                + "' AND description = '" + normBoard.getDescription() + "'";
        long boardId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        taskService.addTaskToBoard(boardId, normTask);
        String sql2 = "SELECT * FROM public.tasks WHERE title = '" + normTask.getTitle()
                + "' AND description = '" + normTask.getDescription() + "'";
        long taskId = (Long) jdbcTemplate.query(sql2, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        normTask.setTitle(null);
        String json = mapper.writeValueAsString(normTask);
        mockMvc.perform(put("/boards/{boardId}/tasks/{taskId}", boardId, taskId).content(json)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isBadRequest());
    }

    @Test
    void validReplaceTaskSameStatus() throws Exception {
        boardService.save(normBoard);
        boardService.save(normBoard2);
        String sql = "SELECT * FROM public.boards WHERE title = '" + normBoard.getTitle()
                + "' AND description = '" + normBoard.getDescription() + "'";
        long boardId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String sql2 = "SELECT * FROM public.boards WHERE title = '" + normBoard2.getTitle()
                + "' AND description = '" + normBoard2.getDescription() + "'";
        long boardId2 = (Long) jdbcTemplate.query(sql2, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String status = "IN_PROGRESS";
        boardService.addStatus(boardId, status);
        boardService.addStatus(boardId2, status);
        normTask.setStatus(status);
        taskService.addTaskToBoard(boardId, normTask);
        String sql3 = "SELECT * FROM public.tasks WHERE title = '" + normTask.getTitle()
                + "' AND description = '" + normTask.getDescription() + "'";
        long taskId = (Long) jdbcTemplate.query(sql3, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        mockMvc.perform(put("/boards/tasks/{taskId}/replace?newBoardId={newBoardId}", taskId, boardId2)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
        Task entry = (Task) jdbcTemplate.query(sql3, (rs, rowNum) -> new Task(rs.getLong("board_id"),
                rs.getString("title"), rs.getString("description"),
                rs.getString("status"), rs.getString("employee"))).toArray()[0];
        normTask.setBoardId(boardId2);
        assertEquals(normTask, entry);
    }

    @Test
    void validReplaceTaskDifferentStatus() throws Exception {
        boardService.save(normBoard);
        boardService.save(normBoard2);
        String sql = "SELECT * FROM public.boards WHERE title = '" + normBoard.getTitle()
                + "' AND description = '" + normBoard.getDescription() + "'";
        long boardId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String sql2 = "SELECT * FROM public.boards WHERE title = '" + normBoard2.getTitle()
                + "' AND description = '" + normBoard2.getDescription() + "'";
        long boardId2 = (Long) jdbcTemplate.query(sql2, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String status = "IN_PROGRESS";
        String status2 = "Another status";
        boardService.addStatus(boardId, status);
        boardService.addStatus(boardId2, status2);
        normTask.setStatus(status);
        taskService.addTaskToBoard(boardId, normTask);
        String sql3 = "SELECT * FROM public.tasks WHERE title = '" + normTask.getTitle()
                + "' AND description = '" + normTask.getDescription() + "'";
        long taskId = (Long) jdbcTemplate.query(sql3, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        mockMvc.perform(put("/boards/tasks/{taskId}/replace?newBoardId={newBoardId}", taskId, boardId2)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
        Task entry = (Task) jdbcTemplate.query(sql3, (rs, rowNum) -> new Task(rs.getLong("board_id"),
                rs.getString("title"), rs.getString("description"),
                rs.getString("status"), rs.getString("employee"))).toArray()[0];
        normTask.setStatus(null);
        normTask.setBoardId(boardId2);
        assertEquals(normTask, entry);
    }

    @Test
    void invalidReplaceTaskNotFound() throws Exception {
        boardService.save(normBoard2);
        String sql = "SELECT * FROM public.boards WHERE title = '" + normBoard2.getTitle()
                + "' AND description = '" + normBoard2.getDescription() + "'";
        long boardId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        long taskId = 0;
        mockMvc.perform(put("/boards/tasks/{taskId}/replace?newBoardId={newBoardId}", taskId, boardId)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Task not found : " + taskId));
    }

    @Test
    void invalidReplaceTaskBoardNotFound() throws Exception {
        boardService.save(normBoard2);
        String sql = "SELECT * FROM public.boards WHERE title = '" + normBoard2.getTitle()
                + "' AND description = '" + normBoard2.getDescription() + "'";
        long boardId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        taskService.addTaskToBoard(boardId, normTask);
        String sql2 = "SELECT * FROM public.tasks WHERE title = '" + normTask.getTitle()
                + "' AND description = '" + normTask.getDescription() + "'";
        long taskId = (Long) jdbcTemplate.query(sql2, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        long boardId2 = 0;
        mockMvc.perform(put("/boards/tasks/{taskId}/replace?newBoardId={newBoardId}", taskId, boardId2)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Board not found : " + boardId2));
    }

    private static Board prepareBoard(String title, String description) {
        return new Board(title, description);
    }

    private static Task prepareTask(String title, String description) {
        Task task = new Task();
        task.setTitle(title);
        task.setDescription(description);
        return task;
    }
}