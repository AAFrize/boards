package com.fintech.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fintech.model.Board;
import com.fintech.model.Task;
import com.fintech.service.BoardService;
import com.fintech.service.TaskService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.hasItems;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class BoardControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private BoardController boardController;
    @Autowired
    private BoardService boardService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final ObjectMapper mapper = new ObjectMapper();

    private Board normBoard = prepareBoard("Board", "Some board");
    private Board normBoard2 = prepareBoard("Board 2", "Some board 2");
    private Task normTask = prepareTask("Task", "Some task");
    private Task normTask2 = prepareTask("Task 2", "Some task 2");

    @AfterEach
    void resetDb() {
        boardService.allBoards().forEach(board -> boardService.delete(board.getId()));
    }

    @Test
    void validAddBoard() throws Exception {
        String json = mapper.writeValueAsString(normBoard);
        mockMvc.perform(post("/boards").content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
        String sql = "SELECT * FROM public.boards WHERE title = '" + normBoard.getTitle()
                + "' AND description = '" + normBoard.getDescription() + "'";
        Board entry = jdbcTemplate.queryForObject(sql, (rs, rowNum) -> new Board(rs.getString("title"),
                rs.getString("description")));
        assertEquals(normBoard, entry);
    }

    @Test
    void invalidAddBoardBadRequest() throws Exception {
        normBoard.setTitle(null);
        String json = mapper.writeValueAsString(normBoard);
        mockMvc.perform(post("/boards").content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    void validGetBoard() throws Exception {
        boardService.save(normBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + normBoard.getTitle()
                + "' AND description = '" + normBoard.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        mockMvc.perform(get("/boards/{boardId}", id).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andExpect(jsonPath("$.title").value(normBoard.getTitle()))
                .andExpect(jsonPath("$.description").value(normBoard.getDescription()));
    }

    @Test
    void invalidGetBoardNotFound() throws Exception {
        long id = 0;
        mockMvc.perform(get("/boards/{boardId}", id).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound()).andExpect(jsonPath("$.message")
                        .value("Board not found : " + id));
    }

    @Test
    void validDeleteBoard() throws Exception {
        boardService.save(normBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + normBoard.getTitle()
                + "' AND description = '" + normBoard.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        mockMvc.perform(delete("/boards/{boardId}", id).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
        var newId = jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray();
        assertTrue(List.of(newId).isEmpty());
    }

    @Test
    void validUpdateBoard() throws Exception {
        boardService.save(normBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + normBoard.getTitle()
                + "' AND description = '" + normBoard.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        normBoard.setDescription("New description");
        String json = mapper.writeValueAsString(normBoard);
        mockMvc.perform(put("/boards/{boardId}", id).content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
        String sql2 = "SELECT * FROM public.boards WHERE id = '" + id + "'";
        Board entry = jdbcTemplate.queryForObject(sql2, (rs, rowNum) -> new Board(rs.getString("title"),
                rs.getString("description")));
        assertEquals(normBoard, entry);
    }

    @Test
    void invalidUpdateBoardBadRequest() throws Exception {
        boardService.save(normBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + normBoard.getTitle()
                + "' AND description = '" + normBoard.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        normBoard.setDescription(null);
        String json = mapper.writeValueAsString(normBoard);
        mockMvc.perform(put("/boards/{boardId}", id).content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    void invalidUpdateBoardNotFound() throws Exception {
        boardService.save(normBoard);
        long id = 0;
        String json = mapper.writeValueAsString(normBoard);
        mockMvc.perform(put("/boards/{boardId}", id).content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound()).andExpect(jsonPath("$.message")
                        .value("Board not found : " + id));
    }

    @Test
    void validAddStatus() throws Exception {
        boardService.save(normBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + normBoard.getTitle()
                + "' AND description = '" + normBoard.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        normBoard.setStatus(Set.of("New status"));
        mockMvc.perform(post("/boards/{boardId}/status?status={status}", id, "New status")
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
        String sql2 = "SELECT * FROM public.boards WHERE id = '" + id + "'";
        Board entry = jdbcTemplate.queryForObject(sql2, (rs, rowNum) -> new Board(rs.getString("title"),
                rs.getString("description")));
        String sqlStatus = "SELECT * FROM public.status_on_boards " +
                "WHERE public.status_on_boards.board_id ='" + id + "'";
        Set<String> status = Set.copyOf(jdbcTemplate.query(sqlStatus,
                (rs, rowNum) -> rs.getString("status")));
        entry.setStatus(status);
        assertEquals(normBoard, entry);
    }

    @Test
    void invalidAddStatusNotFound() throws Exception {
        long id = 0;
        mockMvc.perform(post("/boards/{boardId}/status?status={status}", id, "New status")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Board not found : " + id));
    }

    @Test
    void validAddTaskToBoard() throws Exception {
        boardService.save(normBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + normBoard.getTitle()
                + "' AND description = '" + normBoard.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String status = "New status";
        boardService.addStatus(id, status);
        normTask.setStatus(status);
        String json = mapper.writeValueAsString(normTask);
        boardService.findBoard(id);
        mockMvc.perform(post("/boards/{boardId}/tasks", id).content(json)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
        String sql2 = "SELECT * FROM public.tasks WHERE title = '" + normTask.getTitle()
                + "' AND description = '" + normTask.getDescription() + "'";
        Task entry = jdbcTemplate.queryForObject(sql2, (rs, rowNum) -> new Task(rs.getString("title"),
                rs.getString("description"), rs.getString("status")));
        assertEquals(normTask, entry);
    }

    @Test
    void invalidAddTaskToBoardBadRequest() throws Exception {
        boardService.save(normBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + normBoard.getTitle()
                + "' AND description = '" + normBoard.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        normTask.setTitle(null);
        String json = mapper.writeValueAsString(normTask);
        boardService.findBoard(id);
        mockMvc.perform(post("/boards/{boardId}/tasks", id).content(json)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isBadRequest());
    }

    @Test
    void invalidAddTaskToBoardNotFound() throws Exception {
        long id = 0;
        String json = mapper.writeValueAsString(normTask);
        mockMvc.perform(post("/boards/{boardId}/tasks", id).content(json)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Board not found : " + id));
    }

    @Test
    void validGetAllTasksFromBoard() throws Exception {
        boardService.save(normBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + normBoard.getTitle()
                + "' AND description = '" + normBoard.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        taskService.addTaskToBoard(id, normTask);
        taskService.addTaskToBoard(id, normTask2);
        mockMvc.perform(get("/boards/{boardId}/tasks", id).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andExpect(jsonPath("$..title",
                        hasItems(normTask.getTitle(), normTask2.getTitle())));
    }

    @Test
    void invalidGetAllTasksFromBoardNotFound() throws Exception {
        long id = 0;
        mockMvc.perform(get("/boards/{boardId}/tasks", id).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound()).andExpect(jsonPath("$.message")
                        .value("Board not found : " + id));
    }

    @Test
    void validGetAllBoards() throws Exception {
        boardService.save(normBoard);
        boardService.save(normBoard2);
        mockMvc.perform(get("/boards").contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andExpect(jsonPath("$..title",
                        hasItems(normBoard.getTitle(), normBoard2.getTitle())));
    }

    private static Board prepareBoard(String title, String description) {
        return new Board(title, description);
    }

    private static Task prepareTask(String title, String description) {
        Task task = new Task();
        task.setTitle(title);
        task.setDescription(description);
        return task;
    }
}
