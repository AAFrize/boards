package com.fintech.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fintech.model.Note;
import com.fintech.service.NoteService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class NoteControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private NoteController noteController;
    @Autowired
    private NoteService noteService;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final ObjectMapper mapper = new ObjectMapper();

    private Note normNote = prepareNote("Note", "Some note");
    private Note normNote2 = prepareNote("Note2", "Some note2");

    @AfterEach
    public void resetDb() {
        noteService.allNotes().forEach(note -> noteService.delete(note.getId()));
    }

    @Test
    void validAddNote() throws Exception {
        String json = mapper.writeValueAsString(normNote);
        mockMvc.perform(post("/notes").content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
        String sql = "SELECT * FROM public.notes WHERE title = '" + normNote.getTitle()
                + "' AND description = '" + normNote.getDescription() + "'";
        Note entry = jdbcTemplate.queryForObject(sql, (rs, rowNum) -> new Note(rs.getString("title"),
                rs.getString("description")));
        assertEquals(normNote, entry);
    }

    @Test
    void invalidAddNoteBadRequest() throws Exception {
        normNote.setTitle(null);
        String json = mapper.writeValueAsString(normNote);
        mockMvc.perform(post("/notes").content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    void invalidAddNoteAnotherNoteNotFound() throws Exception {
        long id = 1L;
        normNote.setAnotherNote(id);
        String json = mapper.writeValueAsString(normNote);
        mockMvc.perform(post("/notes").content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest()).andExpect(jsonPath("$.message")
                        .value("Note refers to a non-existent note : " + id));
    }

    @Test
    void validGetNote() throws Exception {
        noteService.save(normNote);
        String sql = "SELECT * FROM public.notes WHERE title = '" + normNote.getTitle()
                + "' AND description = '" + normNote.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        mockMvc.perform(get("/notes/{noteId}", id).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andExpect(jsonPath("$.title").value(normNote.getTitle()))
                .andExpect(jsonPath("$.description").value(normNote.getDescription()));
    }

    @Test
    void invalidGetNoteNotFound() throws Exception {
        long id = 0;
        mockMvc.perform(get("/notes/{noteId}", id).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound()).andExpect(jsonPath("$.message")
                        .value("Note not found : " + id));
    }

    @Test
    void validDeleteNote() throws Exception {
        noteService.save(normNote);
        String sql = "SELECT * FROM public.notes WHERE title = '" + normNote.getTitle()
                + "' AND description = '" + normNote.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        mockMvc.perform(delete("/notes/{noteId}", id).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
        var newId = jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray();
        assertTrue(List.of(newId).isEmpty());
    }

    @Test
    void validUpdateNote() throws Exception {
        noteService.save(normNote);
        noteService.save(normNote2);
        String sql = "SELECT * FROM public.notes WHERE title = '" + normNote.getTitle()
                + "' AND description = '" + normNote.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String sql2 = "SELECT * FROM public.notes WHERE title = '" + normNote2.getTitle()
                + "' AND description = '" + normNote2.getDescription() + "'";
        long id2 = (Long) jdbcTemplate.query(sql2, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        normNote.setDescription("New description");
        normNote.setAnotherNote(id2);
        String json = mapper.writeValueAsString(normNote);
        mockMvc.perform(put("/notes/{noteId}", id).content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
        String sql3 = "SELECT * FROM public.notes WHERE id = '" + id + "'";
        Note entry = jdbcTemplate.queryForObject(sql3, (rs, rowNum) -> new Note(rs.getString("title"),
                rs.getString("description"), rs.getLong("another_note")));
        assertEquals(normNote, entry);
    }

    @Test
    void invalidUpdateNoteBadRequest() throws Exception {
        noteService.save(normNote);
        String sql = "SELECT * FROM public.notes WHERE title = '" + normNote.getTitle()
                + "' AND description = '" + normNote.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        normNote.setDescription(null);
        String json = mapper.writeValueAsString(normNote);
        mockMvc.perform(put("/notes/{noteId}", id).content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    void invalidUpdateNoteAnotherNoteNotFound() throws Exception {
        noteService.save(normNote);
        String sql = "SELECT * FROM public.notes WHERE title = '" + normNote.getTitle()
                + "' AND description = '" + normNote.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        long id2 = 1L;
        normNote.setAnotherNote(id2);
        String json = mapper.writeValueAsString(normNote);
        mockMvc.perform(put("/notes/{noteId}", id).content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest()).andExpect(jsonPath("$.message")
                        .value("Note refers to a non-existent note : " + id2));
    }

    @Test
    void invalidUpdateNoteNotFound() throws Exception {
        long id = 0;
        String json = mapper.writeValueAsString(normNote);
        mockMvc.perform(put("/notes/{noteId}", id).content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound()).andExpect(jsonPath("$.message")
                        .value("Note not found : " + id));
    }

    @Test
    void validGetAllNotes() throws Exception {
        noteService.save(normNote);
        noteService.save(normNote2);
        mockMvc.perform(get("/notes").contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andExpect(jsonPath("$..title",
                        hasItems(normNote.getTitle(), normNote2.getTitle())));
    }

    private static Note prepareNote(String title, String description) {
        return new Note(title, description);
    }

}
