FROM openjdk:17-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} boards-1.jar
ENTRYPOINT ["java","-jar","/boards-1.jar"]